package adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import interfaces.ItemClickListener;
import model.Users;
import tranvanhai.firebasedemo.ChatActivity;
import tranvanhai.firebasedemo.R;
import viewholder.UserViewHolder;

/**
 * Created by Mr Hai on 21/04/2018.
 */

public class UserAdapter extends RecyclerView.Adapter<UserViewHolder>{

    private static final String TUOI = "Tuổi: ";
    private static final String QUE = "Quê: ";

    ArrayList<Users> lstUsers ;
    Context context;
    public UserAdapter(Context context,ArrayList<Users> lstUsers) {
         this.context = context;
         this.lstUsers = lstUsers;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemview = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_user, parent, false);
        UserViewHolder viewHolderStudent = new UserViewHolder(itemview);
        return viewHolderStudent;
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, int position) {

        Users users = lstUsers.get(position);

        holder.tvlstName.setText(users.getFullName());
        holder.tvlstAge.setText(TUOI+ users.getAge());
        holder.tvlstAddress.setText(QUE+ users.getAddress());

        boolean checkstt = users.isOnline();

        if (checkstt){
            holder.btnStatus.setBackgroundResource(R.drawable.custom_online);
        }
        else{
            holder.btnStatus.setBackgroundResource(R.drawable.custom_offline);
        }

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                goToUpdateActivity(lstUsers.get(position).getUserid());
            }
        });

    }

    private void goToUpdateActivity(String personId){
        Intent goToUpdate = new Intent(context, ChatActivity.class);
        goToUpdate.putExtra("USER_ID", personId);
        goToUpdate.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(goToUpdate);
    }

    @Override
    public int getItemCount() {
        return lstUsers.size();
    }

}
