package tranvanhai.firebasedemo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.EventLog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Mr Hai on 20/04/2018.
 */

public class Login extends AppCompatActivity implements View.OnClickListener {

    private static final boolean TRUE = true;
    private static final String LOGIN = "Login";
    private static final String USERID = "userID";
    private static final String ONLINE = "online";

    EditText edUserName,edPassWord;
    Button btnSignIn,btnSignUp;

    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;


    public SharedPreferences share ;
    public SharedPreferences.Editor editor;

    public static ProgressDialog mProgress;

    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(authStateListener);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        firebaseAuth = FirebaseAuth.getInstance();

        boolean checkLogin = checkLogin();
        if (checkLogin) {
            Intent main = new Intent(this, Home.class);
            startActivity(main);
            finish();
        } else {
            setContentView(R.layout.activity_login);
            btnSignIn = findViewById(R.id.btnSignIn);
            btnSignUp = findViewById(R.id.btnSignUp);
            edUserName = findViewById(R.id.edUserName);
            edPassWord = findViewById(R.id.edPassword);
            btnSignUp.setOnClickListener(this);
            btnSignIn.setOnClickListener(this);
        }
        initUI();
    }

    private void initUI() {

        mProgress = new ProgressDialog(this);
        mProgress.setTitle("Login...");
        mProgress.setMessage("Please wait...");
        mProgress.setCancelable(false);
        mProgress.setIndeterminate(true);

        // set-up listener firebaseauth sign-in
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null){
                    //login success
                }
                else {
                    // login fail
                }
            }
        };
    }

    private boolean checkLogin() {

        boolean  check = false;

        //Khởi tạo SharedPreferences có tên "Login"
        SharedPreferences share = getSharedPreferences(LOGIN, MODE_PRIVATE);

        //Lấy chuỗi String trong file SharedPreferences thông qua tên URName và URPass
        String userId = share.getString(USERID, "");

        if (userId.isEmpty()){
            check = false;
        }
        else{
            check = true;
        }
        return check;
    }

    private boolean validateForm(String email,String passWord){
        if (!email.isEmpty() && !passWord.isEmpty()){
            return true;
        }
        else {
            return false;
        }
    }

    private void signIn(String email,String passWord){
        firebaseAuth.signInWithEmailAndPassword(email,passWord)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("users");
                            String userId = firebaseAuth.getCurrentUser().getUid();

                            databaseReference.child(userId).child(ONLINE).setValue(TRUE);
                            databaseReference.child(userId).child(ONLINE).onDisconnect().removeValue();
                            databaseReference.child(userId).child(ONLINE).onDisconnect().setValue(false);

                             share = getSharedPreferences(LOGIN, MODE_PRIVATE);
                             editor = share.edit();

                            editor.putString(USERID,userId);
                            editor.commit();

                            Intent intent = new Intent(Login.this,Home.class);
                            startActivity(intent);
                            mProgress.dismiss();
                            finish();
                            return;
                        }
                        else {
                            mProgress.dismiss();
                            Toast.makeText(getApplicationContext(),"Invalid email or password !",Toast.LENGTH_SHORT).show();
                        }
                     }
                });
    }

    protected void register(String email,String passWord){
        firebaseAuth.createUserWithEmailAndPassword(email,passWord)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            Toast.makeText(getApplicationContext(),"Sign-Up successfull !",Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(getApplicationContext(),"Invalid email or password !",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        String email = edUserName.getText().toString().trim();
        String pass = edPassWord.getText().toString().trim();
        boolean isValid =  validateForm(email,pass);
        switch (id){
            case R.id.btnSignIn:
               if (isValid){
                    signIn(email,pass);
                    mProgress.show();
               }
               else {
                   Toast.makeText(Login.this,"You must enter both email and password",Toast.LENGTH_LONG).show();
               }
                break;
            case R.id.btnSignUp:
                if (isValid){
                    register(email,pass);
                    break;
                }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (authStateListener != null){
            firebaseAuth.removeAuthStateListener(authStateListener);
        }
    }
}
