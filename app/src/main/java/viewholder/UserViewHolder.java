package viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import interfaces.ItemClickListener;
import tranvanhai.firebasedemo.R;

/**
 * Created by Mr Hai on 21/04/2018.
 */

public class UserViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView tvlstName,tvlstAge,tvlstAddress;
    public Button btnStatus;

    private ItemClickListener itemClickListener;


    public UserViewHolder(View itemView) {
        super(itemView);
        tvlstName = itemView.findViewById(R.id.tvlstName);
        tvlstAge = itemView.findViewById(R.id.tvlstAge);
        tvlstAddress =itemView.findViewById(R.id.tvlstAddress);
        btnStatus = itemView.findViewById(R.id.btnStatus);

        itemView.setOnClickListener((View.OnClickListener) this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener)
    {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view,getAdapterPosition(),false);
    }
}
