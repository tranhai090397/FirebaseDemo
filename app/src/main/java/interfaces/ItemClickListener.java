package interfaces;

import android.view.View;

/**
 * Created by Mr Hai on 03/05/2018.
 */

public interface ItemClickListener {

    void onClick(View view, int position, boolean isLongClick);
}
