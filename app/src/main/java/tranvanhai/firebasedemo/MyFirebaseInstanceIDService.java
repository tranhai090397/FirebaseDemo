package tranvanhai.firebasedemo;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {

        String idToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("TAG", "onTokenRefresh: " + idToken);

    }
}
