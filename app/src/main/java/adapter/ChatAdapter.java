package adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

import model.ChatItem;
import tranvanhai.firebasedemo.R;
import viewholder.ChatViewHolder;

public class ChatAdapter extends RecyclerView.Adapter<ChatViewHolder> {

    private static final int  ITEM_TYPE_SENT = 1;
    private static final int ITEM_TYPE_RECEIVED = 2;

    ArrayList<ChatItem> lstChat ;
    Context context;
    public ChatAdapter(Context context,ArrayList<ChatItem> lstChat) {
        this.context = context;
        this.lstChat = lstChat;
    }
    @Override
    public ChatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = null;
        if (viewType == ITEM_TYPE_SENT) {
            v = LayoutInflater.from(context).inflate(R.layout.item_chatsend, parent,false);
        } else if (viewType == ITEM_TYPE_RECEIVED) {
            v = LayoutInflater.from(context).inflate(R.layout.item_chatreceiver, parent,false);
        }
        return new ChatViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ChatViewHolder holder, int position) {
        holder.tvChat.setText(lstChat.get(position).getChatContent());
    }

    @Override
    public int getItemViewType(int position) {
        if (lstChat.get(position).getSenderID().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
            return ITEM_TYPE_SENT;
        } else {
            return ITEM_TYPE_RECEIVED;
        }
    }

    @Override
    public int getItemCount() {
        return lstChat.size();
    }

}
