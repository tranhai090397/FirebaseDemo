package model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mr Hai on 29/04/2018.
 */

public class ChatItem {

    private String senderID;
    private String chatContent;
    private String receiverID;

    public ChatItem() {
    }

    public ChatItem(String senderID, String chatContent, String receiverID) {
        this.senderID = senderID;
        this.chatContent = chatContent;
        this.receiverID = receiverID;
    }

    public String getSenderID() {
        return senderID;
    }

    public void setSenderID(String senderID) {
        this.senderID = senderID;
    }

    public String getChatContent() {
        return chatContent;
    }

    public void setChatContent(String chatContent) {
        this.chatContent = chatContent;
    }

    public String getReceiverID() {
        return receiverID;
    }

    public void setReceiverID(String receiverID) {
        this.receiverID = receiverID;
    }
}