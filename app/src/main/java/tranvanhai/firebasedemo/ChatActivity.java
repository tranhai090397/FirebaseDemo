package tranvanhai.firebasedemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import adapter.ChatAdapter;
import model.ChatItem;
import model.Users;

/**
 * Created by Mr Hai on 29/04/2018.
 **/

public class ChatActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String MESSAGES = "messages";
    private static final String USERS = "users";

    private EditText edChat;
    private Button btnBack;
    private ImageButton btnSend;
    private TextView tvTite;
    private RecyclerView rcvChats;
    private LinearLayoutManager linearLayoutManager;

    private DatabaseReference mMessengerDBRef;
    private DatabaseReference mUserDBRef;

    private ChatAdapter chatAdapter = null;
    private ArrayList<ChatItem> lstChat = new ArrayList<>();

    private String mReceiverName;
    private String mReceiverID;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chats);

        initUI();
    }

    private void initUI() {
        tvTite = findViewById(R.id.tvTv);

        edChat = findViewById(R.id.edChat);

        btnSend = findViewById(R.id.btnSend);
        btnBack = findViewById(R.id.btnBack);
        btnSend.setOnClickListener(this);
        btnBack.setOnClickListener(this);

        rcvChats = findViewById(R.id.rcvChats);
        rcvChats.setHasFixedSize(true);
        rcvChats.setItemAnimator(new DefaultItemAnimator());

        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setStackFromEnd(true);
        rcvChats.setLayoutManager(linearLayoutManager);

        mMessengerDBRef = FirebaseDatabase.getInstance().getReference().child(MESSAGES);
        mUserDBRef = FirebaseDatabase.getInstance().getReference().child(USERS);

        Bundle bundle = getIntent().getExtras();
        if (bundle == null){
            mReceiverID = null;
        }
        else {
            mReceiverID = bundle.getString("USER_ID");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        querymessagesBetweenThisUserAndClickedUser();

        /**sets title bar with recepient name**/
        queryRecipientName(mReceiverID);
    }

    private void queryRecipientName(final String receiverId) {

        mUserDBRef.child(receiverId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Users recepient = dataSnapshot.getValue(Users.class);
                mReceiverName = recepient.getFullName();

                try {
                    tvTite.setText(mReceiverName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void querymessagesBetweenThisUserAndClickedUser() {
        mMessengerDBRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot snap: dataSnapshot.getChildren()){
                    ChatItem chatMessage = snap.getValue(ChatItem.class);
                    if (mReceiverID == null){
                        Toast.makeText(getApplicationContext(),"Rong",Toast.LENGTH_LONG).show();
                        return;
                    }

                    if(chatMessage.getSenderID().equals(FirebaseAuth.getInstance().getCurrentUser().getUid()) && chatMessage.getReceiverID().equals(mReceiverID) || chatMessage.getSenderID().equals(mReceiverID) && chatMessage.getReceiverID().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())){
                        lstChat.add(chatMessage);
                    }
                }

                chatAdapter = new ChatAdapter(getApplicationContext(),lstChat);
                rcvChats.setAdapter(chatAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void sendMessageToFirebase(String senderId, String message, String mReceiverID) {

        lstChat.clear();

        ChatItem newMsg = new ChatItem(senderId,message,mReceiverID);
        mMessengerDBRef.push().setValue(newMsg).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(Task<Void> task) {
                if(!task.isSuccessful()){
                    //error
                    Toast.makeText(ChatActivity.this, "Error " + task.getException().getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(ChatActivity.this, "Message sent successfully!", Toast.LENGTH_SHORT).show();
                    edChat.setText(null);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id){
            case R.id.btnBack:
                Intent intent = new Intent(ChatActivity.this,ListUser.class);
                startActivity(intent);
                break;
            case R.id.btnSend:
                        String message = edChat.getText().toString();
                        String senderId = FirebaseAuth.getInstance().getCurrentUser().getUid();

                        if(message.isEmpty()){
                            Toast.makeText(ChatActivity.this, "You must enter a message", Toast.LENGTH_SHORT).show();
                        }else {
                            //message is entered, send
                            sendMessageToFirebase(senderId, message, mReceiverID);
                        }
                break;
        }
    }
}