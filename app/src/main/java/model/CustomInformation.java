package model;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import tranvanhai.firebasedemo.R;

/**
 * Created by Mr Hai on 26/04/2018.
 */

public class CustomInformation implements GoogleMap.InfoWindowAdapter {

    Activity context;

    public CustomInformation(Activity context) {
        this.context = context;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = context.getLayoutInflater().inflate(R.layout.custom_information, null);

        TextView tvTitle = view.findViewById(R.id.tvTitle);

        tvTitle.setText(marker.getTitle());
        return view;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }
}
