package tranvanhai.firebasedemo;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import adapter.UserAdapter;
import model.Users;

/**
 * Created by Mr Hai on 20/04/2018.
 */

public class ListUser extends AppCompatActivity implements View.OnClickListener {

    RecyclerView rcvListUser;

    private EditText edSearch;
    private ImageButton btnSearch;
    private FloatingActionButton btnMapView;
    private Button btnBack;
    private TextView tvTV;

    private static final String USER = "users";
    private static final String CHILD_KEY = "fullName";

    FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
    DatabaseReference databaseReference = firebaseDatabase.getReference(USER);

    UserAdapter userAdapter;
    ArrayList<Users> lstUsers;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listusers);
        initUI();

    }

    private void initUI() {
        edSearch = findViewById(R.id.edSearch);
        btnSearch = findViewById(R.id.btnSearch);
        btnMapView = findViewById(R.id.btnMapView);
        btnBack = findViewById(R.id.btnBack);
        tvTV = findViewById(R.id.tvTv);
        tvTV.setText("List User");

        btnSearch.setOnClickListener(this);
        btnMapView.setOnClickListener(this);
        btnBack.setOnClickListener(this);
        rcvListUser = findViewById(R.id.rcvListUsers);
        rcvListUser.setHasFixedSize(true);
        rcvListUser.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rcvListUser.setItemAnimator(new DefaultItemAnimator());

        rcvListUser.setFilterTouchesWhenObscured(true);

        lstUsers = new ArrayList<>();
        userAdapter = new UserAdapter(getApplicationContext(),lstUsers);

        rcvListUser.setAdapter(userAdapter);

         //getdata default
        getData();
    }

    private void getData() {
        databaseReference.addChildEventListener(new ChildEventListener() {
           @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
               Users users = dataSnapshot.getValue(Users.class);
                lstUsers.add(users);
                rcvListUser.setAdapter(userAdapter);
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

           @Override
           public void onChildMoved(DataSnapshot dataSnapshot, String s) {
           }

            @Override
           public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btnSearch:
                String keySearch =  edSearch.getText().toString().trim();
                if (keySearch.equals("")){
                    Toast.makeText(getApplicationContext(),"Enter text!!!",Toast.LENGTH_LONG).show();
                }
                else {
                    userSearch(keySearch);
                 }
                break;
            case R.id.btnMapView:
                Intent intent = new Intent(ListUser.this, MapviewListUser.class);
                startActivity(intent);
                break;
            case R.id.btnBack:
                 Intent intents = new Intent(ListUser.this,Home.class);
                    startActivity(intents);
                break;
        }
    }

    private void userSearch(String edSearch) {
        lstUsers.clear();
        rcvListUser.removeAllViews();
        Query query = databaseReference.orderByChild(CHILD_KEY).startAt(edSearch + "\\uf8tt");

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue(Users.class) == null) {
                    Toast.makeText(ListUser.this, "Object null!", Toast.LENGTH_SHORT).show();
                } else {
                    for (DataSnapshot adSnapshot : dataSnapshot.getChildren()) {
                        lstUsers.add(adSnapshot.getValue(Users.class));
                    }
                    rcvListUser.setAdapter(userAdapter);
                    userAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
