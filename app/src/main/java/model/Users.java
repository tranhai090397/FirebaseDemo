package model;

import android.renderscript.BaseObj;

import javax.xml.transform.sax.TemplatesHandler;

/**
 * Created by Mr Hai on 20/04/2018.
 */

public class Users {
    private String fullName;
    private int age;
    private String address;
    private double latitude;
    private double longtitude;
    private String title;
    private String userid;
    private boolean online;

    public Users() {
    }

    public Users(String fullName, int age, String address, double latitude, double longtitude, String title, String userid, boolean online) {
        this.fullName = fullName;
        this.age = age;
        this.address = address;
        this.latitude = latitude;
        this.longtitude = longtitude;
        this.title = title;
        this.userid = userid;
        this.online = online;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(double longtitude) {
        this.longtitude = longtitude;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }
}
