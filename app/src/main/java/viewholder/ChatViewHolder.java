package viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import tranvanhai.firebasedemo.R;

public class ChatViewHolder extends RecyclerView.ViewHolder {

    public TextView tvChat;

    public ChatViewHolder(View itemView) {
        super(itemView);
        tvChat = itemView.findViewById(R.id.tvChat);
    }
}
