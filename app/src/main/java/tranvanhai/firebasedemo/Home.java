package tranvanhai.firebasedemo;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.DragAndDropPermissions;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ThrowOnExtraProperties;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import model.TokenDevice;
import model.Users;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class Home extends AppCompatActivity implements View.OnClickListener,LocationListener{

    private static final String USER = "users";
    private static final String TAG = "Home";
    private static final String TOKENDEVICES = "tokendevices";

    private static final String FULLNAME = "fullName";
    private static final String AGE = "age";
    private static final String ADDRESS = "address";
    private static final String LATITUDE = "latitude";
    private static final String LONGTITUDE = "longtitude";
    private static final String TITLE = "title";
    private static final String USERID = "userid";
    private static final String ONLINE = "online";
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public static final boolean FALSE = false;

    TextView tvName, tvAge, tvAddress;
    Button btnProfile, btnListUser , btnLogOut;
    Dialog dialog;

    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference databaseReference = database.getReference(USER);
    private DatabaseReference reference = database.getInstance().getReference(TOKENDEVICES);


    private FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    private FirebaseUser user = firebaseAuth.getCurrentUser();


    private String idUser;
    private Boolean check = false;
    private Boolean gps_enabled = false;
    private Boolean network_enabled = false;
    private String provider;

    private LocationManager locationManager;

    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        idUser = user.getUid();

        checkLocationService();
        getPermissionLocation();
        getDeviceLocation();
        initUI();

        saveToken();

    }
    private void saveToken() {
        token = FirebaseInstanceId.getInstance().getToken();
        reference.child(idUser).child("userid").setValue(idUser);
        reference.child(idUser).child(token).setValue(token);
    }

    private void getDeviceLocation() {
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location location = locationManager.getLastKnownLocation(provider);
        if (location != null) {
            Toast.makeText(getApplicationContext(), "Provider " + provider + " has been selected.", Toast.LENGTH_LONG).show();
            onLocationChanged(location);
        } else {
            Toast.makeText(getApplicationContext(), "Location not available", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(provider, 400, 1, this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        double lati = location.getLatitude();
        double longt = location.getLongitude();

        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(lati, longt, 1);

            databaseReference.child(idUser).child(LONGTITUDE).setValue(longt);
            databaseReference.child(idUser).child(LATITUDE).setValue(lati);
            databaseReference.child(idUser).child(TITLE).setValue(addresses.get(0).getAddressLine(0));
            databaseReference.child(idUser).child(USERID).setValue(idUser);

        } catch (IOException e) {
            e.printStackTrace();
        }



    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {
        Toast.makeText(this, "Enabled new provider " + provider,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderDisabled(String s) {
        Toast.makeText(this, "Disabled provider " + provider,
                Toast.LENGTH_SHORT).show();
    }

    private void checkLocationService() {

        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        try {
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        }catch (Exception ex){}
        try{
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        }catch (Exception ex){}
        if(!gps_enabled && !network_enabled){
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage(getResources().getString(R.string.gps_network_not_enabled));
            dialog.setPositiveButton(getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    Home.this.startActivity(myIntent);
                }
            });
            dialog.setNegativeButton(getString(R.string.Cancel), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                }
            });
            dialog.show();
        }
    }

    private void getPermissionLocation() {

        if (ActivityCompat.checkSelfPermission(Home.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(Home.this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            check = true;
        } else {
            ActivityCompat.requestPermissions(Home.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
        }
    }

    private void initUI() {
        tvName  = findViewById(R.id.tvName);
        tvAge = findViewById(R.id.tvAge);
        tvAddress = findViewById(R.id.tvAddress);
        btnListUser = findViewById(R.id.btnListUser);
        btnProfile = findViewById(R.id.btnProfle);
        btnLogOut = findViewById(R.id.btnLogout);

        btnProfile.setOnClickListener(this);
        btnListUser.setOnClickListener(this);
        btnLogOut.setOnClickListener(this);

        databaseReference.child(idUser).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Users mUser = dataSnapshot.getValue(Users.class);
                if (mUser == null){
                    Toast.makeText(getApplicationContext(),"No object!",Toast.LENGTH_SHORT).show();
                }
                else {
                    tvName.setText(mUser.getFullName());
                    tvAge.setText(""+mUser.getAge());
                    tvAddress.setText(mUser.getAddress());
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        databaseReference.child(idUser).child(ONLINE).setValue(true);

    }

    private void editProfile() {
        dialog = new Dialog(Home.this);
        dialog.setContentView(R.layout.dialog_editprofile);
        dialog.setTitle("Profile");
        dialog.show();
        final EditText edEditName = dialog.findViewById(R.id.tvEditName);
        final EditText edEditAge = dialog.findViewById(R.id.tvEditAge);
        final EditText edEditAddress = dialog.findViewById(R.id.tvEditAddress);
        final Button btnUpdate  = dialog.findViewById(R.id.btnUpdate);
        Button btnClose = dialog.findViewById(R.id.btnClose);

        edEditName.setText(tvName.getText());
        edEditAge.setText(tvAge.getText());
        edEditAddress.setText(tvAddress.getText());

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String newName = edEditName.getText().toString().trim();
                int newAge = Integer.parseInt(edEditAge.getText().toString().trim());
                String newAddress = edEditAddress.getText().toString().trim();

                updateUser(newName,newAge,newAddress);
                dialog.dismiss();
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
}

    private void updateUser(String newName, int newAge, String newAddress) {

        try{
            if (!TextUtils.isEmpty(newName))
                databaseReference.child(idUser).child(FULLNAME).setValue(newName);

            if (!TextUtils.isEmpty(""+newAge))
                databaseReference.child(idUser).child(AGE).setValue(newAge);

            if (!TextUtils.isEmpty(newAddress))
                databaseReference.child(idUser).child(ADDRESS).setValue(newAddress);

            addUserChangeListener();

        }catch (Exception e){
            Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_LONG).show();
        }

    }

    private void addUserChangeListener() {

        databaseReference.child(idUser).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                    Users user = dataSnapshot.getValue(Users.class);
                    // Check for null
                    if (user == null) {
                        Log.e(TAG, "User data is null!");
                        return;
                    }
                    tvName.setText(user.getFullName());
                    tvAge.setText(""+user.getAge());
                    tvAddress.setText(user.getAddress());
            }
           @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }

    @Override
    public void onClick(View view) {
        int id  = view.getId();
        switch (id){
            case R.id.btnProfle:
                editProfile();
                break;
            case R.id.btnListUser:
                Intent intent = new Intent(Home.this,ListUser.class);
                startActivity(intent);
                break;
            case R.id.btnLogout:
                databaseReference.child(idUser).child(ONLINE).setValue(FALSE);
                SharedPreferences share = getSharedPreferences("Login", MODE_PRIVATE);
                share.edit().clear().commit();

                reference.child(idUser).child(token).removeValue();

                firebaseAuth.signOut();
                Intent intent1 = new Intent(Home.this,Login.class);
                startActivity(intent1);
                break;
        }
    }
}
